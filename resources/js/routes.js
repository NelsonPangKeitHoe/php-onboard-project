import HomePage from './components/HomePage';
import Register from './components/Register';
import Login from './components/Login';
import Deals from './components/Deals'; 
import ListDeals from './components/ListDeals';   
import DeleteDeals from './components/DeleteDeals';

export default[
    {
        path: "/",
        component: HomePage,
        name: 'homepage'
    },
    {
        path: "/register",
        component: Register,
        name: 'register'
    },
    {
        path: "/login",
        component: Login,
        name: 'login'
    },
    {
        path: "/addDeals",
        component: Deals,
        name: "addDeals"
    },
    {
        path: "/listDeals",
        component: ListDeals,
        name: "listDeals"
    },
    {
        path: "/deleteDeals",
        component: DeleteDeals,
        name: "deleteDeals"
    }
]