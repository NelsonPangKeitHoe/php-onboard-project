<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OctaPLuS') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- <script src="https://unpkg.com/vue/dist/vue.js"></script> -->
    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>

    <script type="module">

        import Swiper from 'https://unpkg.com/swiper/swiper-bundle.esm.browser.min.js'

        const swiper = new Swiper('.swiper-container', {
            direction: 'horizontal',
            loop: true,

            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },

            autoplay: {
                delay: 5000,
            },
        });

        const swiper1 = new Swiper('.swiper-container-1', {
            direction: 'horizontal',
            loop: false,
            slidesPerView: 3,
            spaceBetween: 30,

            pagination: {
                el: '.swiper-pagination-1',
                clickable: true,
            },

            autoplay: {
                delay: 5000,
            },
            breakpoints: {
                // when window width is <= 499px
                499: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 30
                },
                // when window width is <= 999px
                999: {
                    slidesPerView: 3,
                    spaceBetweenSlides: 30
                }
            }
        });

        const swiper2 = new Swiper('.swiper-container-2', {
            direction: 'horizontal',
            loop: false,
            slidesPerView: 2,
            spaceBetween: 30,

            pagination: {
                el: '.swiper-pagination-2',
                clickable: true,
            },

            autoplay: {
                delay: 5000,
            },
            breakpoints: {
                // when window width is <= 499px
                499: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 30
                },
                // when window width is <= 999px
                999: {
                    slidesPerView: 2,
                    spaceBetweenSlides: 30
                }
            }
        });

    </script>

    <script> 

        function myFunction() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        window.onclick = function(event) {
            if (!event.target.matches('.dropbtn')) {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
                }
            }
        }

    </script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    
    <style>

        .swiper-container {
            height: 240px;
        }

        .swiper-pagination-bullet{
            border: #FF4500 solid 1px;
            background-color: white;
        }

        .swiper-pagination-bullet-active {
            background-color: #FF4500;
        }

        .text-orange{
            color: #FF4500;
        }

        a:hover{
            color: #FF4500;
        }

    </style>

    <style>

        .header-border-bottom{
            border-bottom: 2px solid #FF4500;
        }

        .margin-auto{
            margin: auto;
        }

        .footer-border-top{
            border-top: 2px outset;
        }

        .footer-padding-top{
            padding-top:40px;
        }

        .follow-us{
            display: inline-block;
            padding-bottom:40px;
        }
        
        .copy-info-background{
            background:#FF4500;
        }

        .copy-info{
            font-size:10px;
        }

        .dropbtn {
        background-color: white;
        color: black;
        padding: 3px;
        font-size: 16px;
        border: none;
        cursor: pointer;
        }

        .dropdown {
        position: relative;
        display: inline-block;
        z-index:10;
        }

        .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 131px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        }

        .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        }

        .show {display:block;}

    </style>

</head>
<body>
    <div id="app">
        <script>
            var BASE_URL = '{{ URL::to('/') }}';
        </script>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
