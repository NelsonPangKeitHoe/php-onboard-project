<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Deals extends Model
{
    protected $connection = 'mongodb';

    protected $fillable = [
        'title', 'image', 'description', 'category',  'price',
    ];
}
