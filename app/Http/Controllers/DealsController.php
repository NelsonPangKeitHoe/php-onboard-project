<?php

namespace App\Http\Controllers;
use App\Deals;

use Illuminate\Http\Request;

class DealsController extends Controller
{
    public function addDeals(Request $request)
    {
        $deals = new Deals();

        $deals->title = $request->title;
        $deals->image = $request->image;
        $deals->description = $request->description;
        $deals->category = $request->category;
        $deals->price = $request->price;
        $deals->save();

        return response()->json($deals, 200);
    }

    public function listDeals()
    {
        $deals = Deals::orderBy('id', 'DESC')->get();
 
        return response()->json([
           'success' => true,
           'data' => $deals
        ]);
    }

    public function deleteDeals(Request $request)
    {
        $deals = Deals::where('id', '=', $request->get('deleteId'))->first();

        $deals->delete();
 
        return response()->json([
            'success' => true
        ]);
    }
}
